# Coding task
Your task is to write a simple React Application that renders list of repositories In the following manner:


        • <name> - 🌟 <stars> - 🍴 <forks>
        • react - 🌟 69012 - 🍴 12581
        • reselect - 🌟 7291 - 🍴 214
        • redux - 🌟 31705 - 🍴 6581
        • recompose - 🌟 5671 - 🍴 342
      
Please use method getReactRepositories fetch list of repositories, once resolved it will return list in following format:

          [
            {name: 'react', stars: 69012, forks: 12581, url: 'http://…'},
            {name: 'react', stars: 7291, forks: 214, url: 'http://…'},
            {name: 'react', stars: 31705, forks: 6581, url: 'http://…'},
            {name: 'react', stars: 5671, forks: 342, url: 'http://…'}
          ]
        

# Bonus Task
Bonus task is to create a higher order component that will then wrap the list of repositories and provide a "More/Less" button that will expanse or collapse the list, for example:

        // Expanded Mode
        +-----------------------------------------------+
        | <name> | 🌟 <numberOfStars> |🍴 <numberOfForks>|        
        +-----------------------------------------------+
        | react      |     🌟 69012      |   🍴 12581    |
        +-----------------------------------------------+
        | reselect    |     🌟 7291       |   🍴 214      |
        +-----------------------------------------------+
        | redux      |     🌟 31705      |   🍴 6581     |
        +-----------------------------------------------+
        | recompose  |     🌟 5671       |   🍴 342      |
        +-----------------------------------------------+
        | See Less Button |
        +-----------------+

        // Collapsed Mode
        +-----------------------------------------------+
        | <name> | 🌟 <numberOfStars> |🍴 <numberOfForks>|        
        +-----------------------------------------------+
        | react      |     🌟 69012      |   🍴 12581    |
        +-----------------------------------------------+
        | reselect    |     🌟 7291       |   🍴 214      |
        +-----------------------------------------------+
        | See More Button |
        +-----------------+
      
## Installation

```bash
yarn install
```

## How to run?

Install all the dependencies first, and them run:

```bash
yarn start
```

## How to run tests?

```bash
yarn test
```

## License

This project is licensed under the MIT license, Copyright (c) 2018 Daniel Kurylo. For more information see `LICENSE.md`.

## Author 

Daniel Kurylo
