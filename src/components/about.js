import React from 'react'
import Typography from 'material-ui/Typography'

const About = () => (
  <div>
    <Typography type="title" gutterBottom>Info</Typography>
    <Typography type="subheading" gutterBottom>
      Hcl recruitment task.
    </Typography>
    <Typography type="body1"><strong>Author:</strong> Daniel Kurylo</Typography>
    <Typography type="body1"><strong>License:</strong> MIT</Typography>
  </div>
)

export default About
