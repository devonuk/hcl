import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import Paper from 'material-ui/Paper';

const styles = theme => ({
  root: {
    width: '100%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
});

const RepositoriesComponent = (props) => {
  const { classes } = props;

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Repository name</TableCell>
            <TableCell numeric>Number of stars</TableCell>
            <TableCell numeric>Number of forks</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.repositories.map(item => {
            return (
              <TableRow key={item.id}>
                <TableCell>{item.name}</TableCell>
                <TableCell numeric>{item.stargazers_count}</TableCell>
                <TableCell numeric>{item.forks}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

RepositoriesComponent.propTypes = {
  classes: PropTypes.object.isRequired,
  repositories: PropTypes.array.isRequired
};

RepositoriesComponent.defaultProps = {
  repositories: []
}
export default withStyles(styles)(RepositoriesComponent);
