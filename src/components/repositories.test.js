import RepositoriesComponent from './repositories'
import { mount } from 'enzyme'
import React from 'react'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()})

describe('<RepositoriesComponent />', () => {

  it('renders the component', () => {

    const container = mount(
      <RepositoriesComponent/>
    )

    expect(container.find(RepositoriesComponent)).toHaveLength(1)
    expect(container.text()).toContain('Repository name')
    expect(container.text()).toContain('Number of stars')
    expect(container.text()).toContain('Number of forks')
  })

  it('renders the list of repositories', () => {

    const repositories = [
      {
        'id': 1,
        'name': 'react',
        'stargazers_count': 1233,
        'forks': 123
      }
    ]

    const container = mount(
      <RepositoriesComponent repositories={repositories}/>
    )

    expect(container.text()).toContain('react')
    expect(container.text()).toContain('1233')
    expect(container.text()).toContain('123')
  })
})
