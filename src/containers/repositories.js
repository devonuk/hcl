import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { getRepositories } from '../reducers/repositories'
import Repositories from '../components/repositories'
import {
  actions
} from '../reducers/repositories'

class RepositoriesContainer extends React.Component {

  componentDidMount () {
    this.props.handleFetchRepositories()
  }

  render () {
    return (
      <div>
        {
          this.props.isLoading ? <div>Loading...</div>
            : <Repositories repositories={this.props.repositories}/>
        }
      </div>
    )
  }
}

const mapStateToProps = state => ({
  repositories: getRepositories(state.repositories),
  isLoading: state.repositories.isLoading
})

const mapDispatchToProps = dispatch => bindActionCreators({
  handleFetchRepositories: actions.loadRepositories,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(RepositoriesContainer)
