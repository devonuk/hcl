import React from 'react'
import Repositories from './repositories'
import { shallow, mount } from 'enzyme'
import { configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { MemoryRouter } from 'react-router-dom'
import { createMockStore } from 'redux-test-utils'
import RepositoriesComponent from '../components/repositories'

configure({adapter: new Adapter()})

describe('<Repositories />', () => {

  it('renders component', () => {

    const container = shallow(
      <MemoryRouter>
        <Repositories/>
      </MemoryRouter>
    )

    expect(container.find(Repositories)).toHaveLength(1)
  })

  it('renders movie details', () => {

    const repositoriesStore = {
      repositories: {
        isLoading: false,
        repositories: [
          {
            'id': 1,
            'name': 'react',
            'stargazers_count': 1233,
            'forks': 123
          }
        ]
      }
    }

    const store = createMockStore(repositoriesStore)

    const container = mount(
      <MemoryRouter>
        <Repositories store={store}/>
      </MemoryRouter>
    )

    expect(container.find(RepositoriesComponent)).toHaveLength(1)
  })

  it('renders loading when fetching data', () => {

    const repositoriesStore = {
      repositories: {
        isLoading: true,
        repositories: []
      }
    }

    const store = createMockStore(repositoriesStore)

    const container = mount(
      <MemoryRouter>
        <Repositories store={store}/>
      </MemoryRouter>
    )

    expect(container.text()).toContain('Loading...')
  })
})
