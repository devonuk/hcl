import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import app from './app'
import repositories from './repositories'

export default combineReducers({
  routing: routerReducer,
  app,
  repositories
})
