export const types = {
  LOAD_REPOSITORIES_REQUEST: 'repositories/LOAD_REPOSITORIES_REQUEST',
  LOAD_REPOSITORIES_REQUEST_SUCCESS: 'repositories/LOAD_REPOSITORIES_REQUEST_SUCCESS',
}

const initialState = {
  isLoading: false,
  repositories: []
}

export default function reducers (state = initialState, action) {
  switch (action.type) {
    case types.LOAD_REPOSITORIES_REQUEST:
      return {
        ...state,
        isLoading: true
      }

    case types.LOAD_REPOSITORIES_REQUEST_SUCCESS:

      return {
        ...state,
        repositories: action.payload.data.items,
        isLoading: !state.isLoading
      }

    default:
      return state
  }
}

export const getRepositories = (state) => {
  return state.repositories
}

export const actions = {
  loadRepositories: () => ({
    type: types.LOAD_REPOSITORIES_REQUEST,
    payload: {
      request: {
        url: 'https://api.github.com/search/repositories?q=react'
      }
    }
  }),
}
