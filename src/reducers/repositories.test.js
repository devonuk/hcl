import reducer, { actions } from './repositories'

import configureStore from 'redux-mock-store'
import axios from 'axios'
import axiosMiddleware from 'redux-axios-middleware'
import MockAdapter from 'axios-mock-adapter'

describe('Search reducer', () => {

  it('should set isLoading flag', () => {

    let initialState = {
      isLoading: false,
      repositories: []
    }

    const action = actions.loadRepositories()

    const newState = reducer(initialState, action)
    expect(newState.isLoading).toEqual(true)
  })

  it('should load suggestions by ajax', async () => {

    const client = axios.create()
    const axiosClientMock = new MockAdapter(client)

    let response = {
      'items': [
        {
          'id': 1,
          'name': 'react',
          'stargazers_count': 1233,
          'forks': 123
        }
      ]
    }

    axiosClientMock.onGet('https://api.github.com/search/repositories?q=react').reply(200, response)

    const mockStore = configureStore([axiosMiddleware(axiosClientMock.axiosInstance)])
    const store = mockStore({})

    store.dispatch(actions.loadRepositories()).then(() => {
        const storeActions = store.getActions()

        expect(storeActions[0]).toEqual(actions.loadRepositories())
        expect(storeActions[1].payload.data).toEqual(response)

        const state = {}
        const data = reducer(state, storeActions[1])

        expect(data.repositories[0].id).toBe(1)
        expect(data.repositories[0].name).toBe('react')
        expect(data.repositories[0].stargazers_count).toBe(1233)
        expect(data.repositories[0].forks).toBe(123)
      }
    )
  })
})
